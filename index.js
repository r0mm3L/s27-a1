const http = require('http');

const PORT = 4000;

http.createServer( function(request, response) {

	if (request.url === "/"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('welcome to booking system')
	} 

	else if(request.url == "/profile" && request.method === "GET") {

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('welcome to your profile')
	}

	else if(request.url == "/courses" && request.method === "GET"){

	

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Heres our courses available')

	}

	else if (request.url === "/addcourse" && request.method === "POST") {
		
		
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Add a course to our reesources")

		
	}

	else if (request.url === "/updatecourse" && request.method === "PUT"){



	    response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("update a course to our resources")


	}
	else if (request.url === "/archivecourses" && request.method === "DELETE") {
		
	
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Archive a course to our resources")

		
	}


}).listen(PORT)

console.log(`Server is running at localhost:${PORT}`)